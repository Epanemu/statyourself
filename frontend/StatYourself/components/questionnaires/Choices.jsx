import React, {useState} from 'react'
import { StyleSheet } from "react-native";
import { View, Text, TextInput, Button } from '../Themed'
import { FlatList } from 'react-native';

export function Choices(props) {

    var renderItem = ({ item,  index}) => {
        return <TextInput
            // value={item}
            style={styles.choiceInput}
            onChangeText={text => {
                props.choices[index] = text;
                props.setChoices(props.choices);
            }}
        />
    };

    return <FlatList
                ListHeaderComponent={<Text style={styles.header}>Choices:</Text>}
                data={props.choices}
                renderItem={renderItem}
                keyExtractor={(c, i) => i.toString()}
                ListFooterComponent={
                    <Button
                        onPress={props.addChoice}
                        title="Add a choice"
                        accessibilityLabel="Add a choice"
                    />}
            />
}

const styles = StyleSheet.create({
    choiceInput: {
        fontSize: 15,
        padding: 4,
        marginBottom: 10,
    },
    header: {
        fontSize: 15,
        marginBottom: 10,
    }
})