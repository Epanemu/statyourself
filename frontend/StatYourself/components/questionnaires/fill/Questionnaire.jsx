import React from 'react'
import { StyleSheet } from "react-native";
import { View, Text, TextInput, Button } from '../../Themed'

export function Questionnaire(props) {

    console.log(props);

    return <View>
        <Text style={styles.header}>{props.questionnaire.name}</Text>
    </View>
}

const styles = StyleSheet.create({
    choiceInput: {
        fontSize: 15,
        padding: 4,
        marginBottom: 10,
    },
    header: {
        fontSize: 20,
        marginBottom: 10,
    }
})