import React from 'react'
import { SectionList, StyleSheet } from "react-native";
import { TextInput, Button } from '../Themed'
import { Question } from './Question'

export function ModuleList(props) {
    return <SectionList
            sections={props.modules}
            keyExtractor={(question, index) => question.id}
            renderItem={({ item }) => <Question question={item} />}
            renderSectionHeader={({section}) => (
                <TextInput
                  placeholder={'Name of the Section'}
                  style={styles.nameInput}
                  onChangeText={text => {
                        props.modules[section.order].name = text;
                        props.setModules(props.modules)
                    }}
                />
            )}
            renderSectionFooter={ ({section}) => (
                <Button
                    style={styles.addBtn}
                    onPress={() => props.addQuestion(section.order)}
                    title="Add a new Question"
                    accessibilityLabel="Add a new Question"
                />
            )}
            ListFooterComponent={<Button
                style={styles.lastBtn}
                onPress={props.addModule}
                title="Add a new Section"
                accessibilityLabel="Add a new Section"
            />}
        />
}

const styles = StyleSheet.create({
    nameInput: {
        fontSize: 20,
        padding: 4,
        marginBottom: 10,
    },
    addBtn: {
        marginBottom: 20,
    },
    lastBtn: {
    },
})