import React from 'react'
import { StyleSheet } from "react-native";
import { View, Text } from '../Themed'

export function Question(props) {
    if (props.question.choices)
        return <View>
            <Text>{props.question.question} ({props.question.choices.map((c, i, a) => i==0 ? c : ", "+c)})</Text>
        </View>
    else
        return <View>
            <Text>{props.question.question}</Text>
        </View>
}