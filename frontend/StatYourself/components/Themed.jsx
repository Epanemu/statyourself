import * as React from 'react';
import { Text as DefaultText, View as DefaultView, Button as DefaultButton, TextInput as DefaultTextInput } from 'react-native';

import Colors from '../constants/Colors';
// import useColorScheme from '../hooks/useColorScheme';

export function useThemeColor( props, colorName) {
  const theme = 'dark';
  // const theme = useColorScheme();
  const colorFromProps = props[theme];

  if (colorFromProps) {
    return colorFromProps;
  } else {
    return Colors[theme][colorName];
  }
}

export function Text(props) {
  const { style, lightColor, darkColor, ...otherProps } = props;
  const color = useThemeColor({ light: lightColor, dark: darkColor }, 'text');

  return <DefaultText style={[{ color }, style]} {...otherProps} />;
}

export function View(props) {
  const { style, lightColor, darkColor, ...otherProps } = props;
  const backgroundColor = useThemeColor({ light: lightColor, dark: darkColor }, 'background');

  return <DefaultView style={[{ backgroundColor }, style]} {...otherProps} />;
}

export function Button(props) {
  return <DefaultButton color="#555555" {...props} />;
}

export function TextInput(props) {
  const { style, ...otherProps } = props;
  const backgroundColor= 'black';
  const color= 'white';

  return <DefaultTextInput style={[{ color, backgroundColor }, style]} {...otherProps} />;
}
