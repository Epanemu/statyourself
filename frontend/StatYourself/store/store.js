import { configureStore } from '@reduxjs/toolkit'

import questionnairesReducer from '../screens/questionnaires/questionnairesSlice'

export default configureStore({
  reducer: {
    questionnaires: questionnairesReducer
  }
})