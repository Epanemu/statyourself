const url = "http://192.168.1.58:8000/api/";

export const API = {
    questionnaires: url+"questionnaire/",
    questionnaires_extended: url+"questionnaire/extended/",
    answerTypes: url+"answer_type/",
}