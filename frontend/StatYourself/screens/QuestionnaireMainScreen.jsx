import React, {useEffect, useState} from 'react';
import { useSelector, useDispatch } from 'react-redux'
import { ActivityIndicator, FlatList, StyleSheet, TouchableOpacity, ScrollView } from 'react-native';

import { questionnairesSet } from './questionnaires/questionnairesSlice'
import { Text, View } from '../components/Themed';
import { API } from '../constants/Configuration'

export default function QuestionnaireMainScreen({ navigation }) {
  const [isLoading, setLoading] = useState(true);

  const dispatch = useDispatch()

  useEffect(() => {
    fetch(API.questionnaires)
      .then((response) => response.json())
      .then((json) => {
        console.log(json.results)

        dispatch(
          questionnairesSet(json.results)
        )
      })
      .catch((error) => console.error(error))
      .finally(() => setLoading(false));
  }, []);


  const questionnaires = useSelector(state => state.questionnaires)

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Questionnaires</Text>
      {isLoading ? <ActivityIndicator/> : (
        <ScrollView style={{width: "100%"}}>
          <FlatList
            data={questionnaires}
            keyExtractor={({ id }, index) => id}
            renderItem={({ item }) => (
              <TouchableOpacity onPress={() => {navigation.navigate('FillQuestionnaire', {id:item.id})}}>
                <View style={styles.questionnaire_form} key={item.id}>
                  <Text style={styles.q_title}>{item.name}</Text>
                </View>
              </TouchableOpacity>
            )}
          />
        </ScrollView>
      )}
      <TouchableOpacity style={styles.fab} onPress={() => {navigation.navigate('CreateQuestionnaire')}}>
        <Text style={styles.fab_text}>+</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    padding: 20,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  fab:{
    height: 50,
    width: 50,
    borderRadius: 100,
    padding:0,
    position: 'absolute',
    bottom: 20,
    right: 20,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor:'#555555',
  },
  fab_text:{
    margin:0,
    padding:0,
    fontSize:30,
    color:'white'
  },
  questionnaire_form: {
    padding: 5,
    margin: 5,
    borderWidth: 2,
    borderColor: '#555555',
  },
  q_title: {
    fontSize: 15,
    fontWeight: 'bold',
  },
});
