import { createSlice } from '@reduxjs/toolkit'
import { API } from '../../constants/Configuration'

const initialState = []

const answersSlice = createSlice({
  name: 'answers',
  initialState,
  reducers: {
    sendAnswers(state, action) {
      let data = action.payload
      console.log(data)

      // fetch(API.questionnaires_extended, {
      //     method: 'POST',
      //     headers: {
      //       'Accept': 'application/json',
      //       'Content-Type': 'application/json'
      //     },
      //     body: JSON.stringify(data)
      //   })
      // .then((response) => response.json())
      // .then((json) => console.log(json))
      // .catch((error) => console.error(error));
    },
    setAnswer(state, action) {
      console.log(state)
      // state.push(...action.payload);
    },
  }
})

export const { sendAnswers, setAnswer } = answersSlice.actions

export default answersSlice.reducer