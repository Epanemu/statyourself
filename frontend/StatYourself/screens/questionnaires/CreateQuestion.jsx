import React, { useState, useEffect } from 'react'
import { StyleSheet, Button } from 'react-native';
import { Picker } from '@react-native-community/picker';
import { API } from '../../constants/Configuration'

import { View, TextInput } from '../../components/Themed';
import { Choices } from '../../components/questionnaires/Choices';

export default function CreateQuestion({ route, navigation }) {

  const { moduleId } = route.params
  const [question, setQuestion] = useState('')
  const [answerType, setAnswerType] = useState('')
  const [answerTypesPicker, setAnswerTypesPicker] = useState([])
  const [choices, setChoices] = useState([])
  const [showChoices, setShowChoices] = useState(false)

  const onSaveClicked = () => {
    if (question && answerType) {
      // either choices are not shown, or there are at least two
      console.log(choices)
      if (!showChoices)
        navigation.navigate('CreateQuestionnaire', { moduleId, question, answerType})
      else if (choices.length > 1)
        navigation.navigate('CreateQuestionnaire', { moduleId, question, answerType, choices})
    }
  }

  const setupPicker = (data) => {
    let types = data.map(t => <Picker.Item  key={t.id} label={t.verbose_name} value={t.name} />)
    setAnswerTypesPicker([<Picker.Item key="0" label="Pick a type" value="" />, ...types]);
  }

  //  for i in fetched types put them to an array
  useEffect(() => {
    fetch(API.answerTypes)
      .then((response) => response.json())
      .then((json) => setupPicker(json.results))
      .catch((error) => console.error(error))
  }, []);


  return (
    <View style={styles.container}>
      <TextInput
        placeholder={'Your question'}
        style={styles.nameInput}
        onChangeText={text => setQuestion(text)}
        value={question}
      />
      <Picker
        selectedValue={answerType}
        onValueChange={(answer, itemIndex) => {
          setAnswerType(answer)
          if (answer.includes('choice'))
            setShowChoices(true);
          else
            setShowChoices(false);
        }}>
        {answerTypesPicker}
      </Picker>
      {showChoices ?
        (<Choices
          setChoices={setChoices}
          addChoice={() => {setChoices([...choices, '']); console.log(choices)}}
          choices={choices}
        />) : (<></>)}
      <Button
        onPress={onSaveClicked}
        style={styles.saveBtn}
        title="Save"
        color="#5500dd"
        accessibilityLabel="Save the question"
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 20,
    fontSize: 16,
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  nameInput: {
    fontSize: 20,
    padding: 4,
  },
});