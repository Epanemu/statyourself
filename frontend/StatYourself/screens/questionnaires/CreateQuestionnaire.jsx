import React, { useState } from 'react'
import { StyleSheet, Button } from 'react-native';
import { useDispatch } from 'react-redux'

import { newQuestionnaireAdded } from './questionnairesSlice'

import { Text, View } from '../../components/Themed';
import { ModuleList } from '../../components/questionnaires/ModuleList'
import { TextInput } from 'react-native-gesture-handler';

export default function CreateQuestionnaire({ route, navigation }) {
  const [name, setName] = useState('')
  const [modules, setModules] = useState([])

  React.useEffect(() => {
    if (route.params?.question && route.params?.answerType) {
      const { moduleId, question, answerType } = route.params

      let temp = [...modules]
      if (route.params.choices) {
        const { choices } = route.params
        temp[moduleId].data.push({ question, answerType, choices });
      } else {
        temp[moduleId].data.push({ question, answerType });
      }
      setModules(temp)
    }
  }, [route.params?.moduleId, route.params?.question, route.params?.answerType]);

  const dispatch = useDispatch()

  const onSaveClicked = () => {
    if (name && modules.length > 0 && !modules.some((m, i, array) => (m.name === '' || m.data.length === 0))) {

      dispatch(
        newQuestionnaireAdded({
          name,
          modules,
        })
      )

      navigation.navigate('QuestionnaireMainScreen')
    }
  }

  return (
    <View style={styles.container}>
      <TextInput
        placeholder={'Name of the Questionnaire'}
        style={styles.nameInput}
        onChangeText={text => setName(text)}
        value={name}
      />
      <View style={styles.separator} lightColor="#eee" darkColor="rgba(255,255,255,0.1)" />
      <ModuleList
        style={styles.moduleList}
        modules={modules}
        addModule={() => setModules([...modules, {name:'', order:modules.length, data:[]}])}
        setModules={setModules}
        addQuestion={(moduleId) => navigation.push('CreateQuestion', { moduleId })}
      />
      <Button
        onPress={onSaveClicked}
        style={styles.saveBtn}
        title="Save"
        color="#5500dd"
        accessibilityLabel="Save the questionnaire"
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 20,
    fontSize: 16,
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  moduleList: {
    flexGrow: 1,
  },
  nameInput: {
    fontSize: 30,
    padding: 4,
    backgroundColor: 'black',
    color: 'white',
  },
  separator: {
    marginVertical: 20,
    height: 1,
    width: '100%',
  },
  saveBtn: {
    alignSelf: 'flex-end',
  }
});