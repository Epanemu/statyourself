import React, { useEffect, useState } from 'react'
import { StyleSheet, Button, ScrollView, ActivityIndicator } from 'react-native';
import { useDispatch } from 'react-redux'

import { sendAnswers } from './answersSlice'

import { Questionnaire } from '../../components/questionnaires/fill/Questionnaire'
import { View } from '../../components/Themed';
import { API } from '../../constants/Configuration'

export default function FillQuestionnaire({ route, navigation }) {
  const [questionnaire, setQuestionnaire] = useState(null)
  const [loading, setLoading] = useState(true)

  const dispatch = useDispatch()

  var questions_n = 0;

  useEffect(() => {
    fetch(API.questionnaires + route.params.id + '/')
      .then((response) => response.json())
      .then((json) => {
        console.log(json)
        setQuestionnaire(json)
      })
      .catch((error) => console.error(error))
      .finally(() => setLoading(false))
  }, []);

  const onSendClicked = () => {
    if (answers.length != questions_n)
      return;
    for (let i = 0; i < answers.length; i++) {
      if (answers[i].answer === '' || answers[i].answer === [])
        return;
    }

    dispatch(
      sendAnswers(answers)
    )

    navigation.navigate('QuestionnaireMainScreen')
  }


  return (
    <View style={styles.container}>
      <ScrollView style={styles.container}>
        {loading ? <ActivityIndicator /> :
          <Questionnaire questionnaire={questionnaire} />}
        <Button
          onPress={onSendClicked}
          style={styles.sendBtn}
          title="Send"
          color="#5500dd"
          accessibilityLabel="Send the answers"
        />
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 10,
    width: "100%",
    height: "100%",
  },
  sendBtn: {
  }
});