import { createSlice } from '@reduxjs/toolkit'
import { API } from '../../constants/Configuration'

const initialState = []

const questionnairesSlice = createSlice({
  name: 'questionnaires',
  initialState,
  reducers: {
    newQuestionnaireAdded(state, action) {
      let data = action.payload
      data.modules = action.payload.modules.map(m => ({ name: m.name, questions: m.data}))
      // console.log(data)

      fetch(API.questionnaires_extended, {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(data)
        })
      .then((response) => response.json())
      .then((json) => console.log(json))
      .catch((error) => console.error(error));

      state.push(action.payload)
    },
    questionnairesSet(state, action) {
      console.log(action);
      state.push(...action.payload);
    },
  }
})

export const { questionnairesSet, newQuestionnaireAdded } = questionnairesSlice.actions

export default questionnairesSlice.reducer