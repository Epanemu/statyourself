import * as Linking from 'expo-linking';

export default {
  prefixes: [Linking.makeUrl('/')],
  config: {
    screens: {
      Root: {
        screens: {
          Overview: {
            screens: {
              OverviewScreen: 'Overview',
            },
          },
          Tasks: {
            screens: {
              TaskMainScreen: 'Tasks',
            },
          },
          Questionnaires: {
            screens: {
              QuestionnaireMainScreen: 'Questionnaires',
              CreateQuestionnaire: 'CreateQuestionnaire',
              CreateQuestion: 'CreateQuestion',
              FillQuestionnaire: 'FillQuestionnaire',
            },
          },
        },
      },
      NotFound: '*',
    },
  },
};
