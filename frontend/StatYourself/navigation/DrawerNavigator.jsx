import { Ionicons } from '@expo/vector-icons';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createStackNavigator } from '@react-navigation/stack';
import * as React from 'react';

import Colors from '../constants/Colors';
import TaskMainScreen from '../screens/TaskMainScreen';
import OverviewScreen from '../screens/OverviewScreen';
import QuestionnaireMainScreen from '../screens/QuestionnaireMainScreen';
import CreateQuestionnaire from '../screens/questionnaires/CreateQuestionnaire';
import CreateQuestion from '../screens/questionnaires/CreateQuestion';
import FillQuestionnaire from '../screens/questionnaires/FillQuestionnaire';
// import { useColorScheme } from 'react-native';
import Layout from '../constants/Layout'

const Drawer = createDrawerNavigator();

export default function DrawerNavigator() {
  const colorScheme = 'dark';
  // const colorScheme = useColorScheme();


  return (
    <Drawer.Navigator
      initialRouteName="Overview"
      backBehavior="history"
      drawerType= { Layout.isLargeDevice ? 'permanent' : 'front' }
      drawerContentOptions={{ activeTintColor: Colors[colorScheme].tint }}
    >
      <Drawer.Screen
        name="Overview"
        component={OverviewNavigator}
        options={{
          drawerIcon: ({ color }) => <DrawerIcon name="ios-code" color={color} />,
        }}
      />
      <Drawer.Screen
        name="Tasks"
        component={TasksNavigator}
        options={{
          drawerIcon: ({ color }) => <DrawerIcon name="ios-code" color={color} />,
        }}
      />
      <Drawer.Screen
        name="Questionnaires"
        component={QuestionnairesNavigator}
        options={{
          drawerIcon: ({ color }) => <DrawerIcon name="ios-code" color={color} />,
        }}
      />
    </Drawer.Navigator>
  );
}

// You can explore the built-in icon families and icons on the web at:
// https://icons.expo.fyi/
function DrawerIcon(props) {
  return <Ionicons size={30} style={{ marginBottom: -3 }} {...props} />;
}

// Each tab has its own navigation stack, you can read more about this pattern here:
// https://reactnavigation.org/docs/tab-based-navigation#a-stack-navigator-for-each-tab
const OverviewStack = createStackNavigator();

function OverviewNavigator() {
  return (
    <OverviewStack.Navigator>
      <OverviewStack.Screen
        name="OverviewScreen"
        component={OverviewScreen}
      />
    </OverviewStack.Navigator>
  );
}

const TasksStack = createStackNavigator();

function TasksNavigator() {
  return (
    <TasksStack.Navigator>
      <TasksStack.Screen
        name="TaskMainScreen"
        component={TaskMainScreen}
        options={{ headerTitle: 'Tasks' }}
      />
    </TasksStack.Navigator>
  );
}

const QuestionnairesStack = createStackNavigator();

function QuestionnairesNavigator() {
  return (
    <QuestionnairesStack.Navigator>
      <QuestionnairesStack.Screen
        name="QuestionnaireMainScreen"
        component={QuestionnaireMainScreen}
        options={{ headerTitle: 'Questionnaires' }}
      />
      <QuestionnairesStack.Screen
        name="CreateQuestionnaire"
        component={CreateQuestionnaire}
        options={{ headerTitle: 'Questionnaires' }}
      />
      <QuestionnairesStack.Screen
        name="CreateQuestion"
        component={CreateQuestion}
        options={{ headerTitle: 'Questionnaires' }}
      />
      <QuestionnairesStack.Screen
        name="FillQuestionnaire"
        component={FillQuestionnaire}
        options={{ headerTitle: 'Questionnaires' }}
      />
    </QuestionnairesStack.Navigator>
  );
}
