from django.db import models
from . import LimitType, Question
from utils import remove_exponent

class Limit(models.Model):
    question = models.ForeignKey(Question, models.CASCADE)
    inverted = models.BooleanField()
    limit_type = models.ForeignKey(LimitType, models.DO_NOTHING)

    def __str__(self):
        return f"{self.limit_type} ({self.question.id})"


class LimitRegex(models.Model):
    q_limit = models.OneToOneField(Limit, models.CASCADE, primary_key=True)
    regex = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
       verbose_name = 'Regex Limit'
       verbose_name_plural = 'Regex Limits'

    def __str__(self):
        return f"{self.regex}"

class LimitRange(models.Model):
    q_limit = models.OneToOneField(Limit, models.CASCADE, primary_key=True)
    lowest = models.DecimalField(max_digits=15, decimal_places=5, blank=True, null=True)
    highest = models.DecimalField(max_digits=15, decimal_places=5, blank=True, null=True)
    step = models.DecimalField(max_digits=15, decimal_places=5, blank=True, null=True)

    class Meta:
       verbose_name = 'Range Limit'
       verbose_name_plural = 'Range Limits'

    def __str__(self):
        s = "["
        if self.lowest is not None:
            s += f"{remove_exponent(self.lowest)}, "
        else:
            s += "-, "
        if self.highest is not None:
            s += f"{remove_exponent(self.highest)}]"
        else:
            s += f"-]"
        if self.step is not None:
            s += f" ({remove_exponent(self.step)})"
        return s


class LimitDurationRange(models.Model):
    q_limit = models.OneToOneField(Limit, models.CASCADE, primary_key=True)
    lowest = models.TimeField(blank=True, null=True)
    highest = models.TimeField(blank=True, null=True)
    step = models.TimeField(blank=True, null=True)

    class Meta:
       verbose_name = 'Duration Range Limit'
       verbose_name_plural = 'Duration Range Limits'

    def __str__(self):
        s = "["
        if self.lowest is not None:
            s += f"{self.lowest}, "
        else:
            s += "-, "
        if self.highest is not None:
            s += f"{self.highest}]"
        else:
            s += f"-]"
        if self.step is not None:
            s += f" ({self.step})"
        return s


class LimitTimeRange(models.Model):
    q_limit = models.OneToOneField(Limit, models.CASCADE, primary_key=True)
    lowest = models.DateTimeField(blank=True, null=True)
    highest = models.DateTimeField(blank=True, null=True)

    class Meta:
       verbose_name = 'Time Range Limit'
       verbose_name_plural = 'Time Range Limits'

    def __str__(self):
        s = "["
        if self.lowest is not None:
            s += f"{self.lowest}, "
        else:
            s += "-, "
        if self.highest is not None:
            s += f"{self.highest}]"
        else:
            s += f"-]"
        return s
