from django.db import models
from users.models import Account
from task_manager.models import Recurrence, Subject

class AnswerType(models.Model):
    name = models.CharField(max_length=255, unique=True)
    verbose_name = models.CharField(max_length=255)
    description = models.TextField(blank=True, null=True)

    def __str__(self):
        return f"{self.name}"


class LimitType(models.Model):
    a_types = models.ManyToManyField(AnswerType, related_name='limit_types', related_query_name="limit_type")
    name = models.CharField(max_length=255)
    description = models.TextField(blank=True, null=True)

    def __str__(self):
        return f"{self.name}"


class Questionaire(models.Model):
    creator = models.ForeignKey(Account, models.DO_NOTHING)
    name = models.CharField(max_length=255)
    description = models.TextField(blank=True, null=True)
    date_created = models.DateTimeField(blank=True, null=True, auto_now_add=True)
    active = models.BooleanField(default=True)
    users = models.ManyToManyField(Account, related_name="questionnaire", related_query_name="questionnaire", through='QuestionaireUse')

    def __str__(self):
        return f"{self.name}"


class QuestionaireUse(models.Model):
    account = models.ForeignKey(Account, models.CASCADE)
    questionnaire = models.ForeignKey(Questionaire, models.CASCADE)
    recurrence = models.ForeignKey(Recurrence, models.DO_NOTHING)
    date_started = models.DateTimeField(blank=True, null=True)
    active = models.IntegerField()
    notifications = models.IntegerField(blank=True, null=True)

    class Meta:
        unique_together = (('account', 'questionnaire'),)

    def __str__(self):
        return f"{self.account} - {self.questionnaire}"


class Module(models.Model):
    questionnaire = models.ForeignKey(Questionaire, models.DO_NOTHING)
    name = models.CharField(max_length=255)
    parent_module = models.ForeignKey('self', models.SET_NULL, blank=True, null=True)
    subject = models.ForeignKey(Subject, models.DO_NOTHING, blank=True, null=True)
    c_block = models.ForeignKey('ConditionBlock', models.DO_NOTHING, blank=True, null=True)
    active = models.BooleanField(default=True)
    order = models.IntegerField()

    def __str__(self):
        return f"{self.questionnaire} ({self.order})"


class Question(models.Model):
    module = models.ForeignKey(Module, models.DO_NOTHING)
    c_block = models.ForeignKey('ConditionBlock', models.DO_NOTHING, blank=True, null=True)
    a_type = models.ForeignKey(AnswerType, models.DO_NOTHING)
    active = models.BooleanField(default=True)
    order = models.IntegerField()
    question = models.TextField()
    importance = models.IntegerField()

    def __str__(self):
        return f"{self.question}"


class QuestionChoice(models.Model):
    question = models.ForeignKey(Question, models.CASCADE)
    name = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return f"{self.name}"
