from django.db import models
from . import Question, QuestionChoice, AnswerType
from utils import remove_exponent

class ConditionBlock(models.Model):
    negation = models.BooleanField(default=False)
    part_of = models.ForeignKey('Junction', models.CASCADE, blank=True, null=True)
    condition = models.ForeignKey('Condition', models.CASCADE, blank=True, null=True)

    def __str__(self):
        if self.condition is not None:
            return f"{self.condition} block"
        return f"part of {self.part_of}"


class Junction(models.Model):
    c_block = models.OneToOneField(ConditionBlock, models.CASCADE, primary_key=True)
    c_type = models.CharField(max_length=10, choices=(('AND', 'AND'), ('OR', 'OR')))

    def __str__(self):
        return f"{self.c_block} ({self.c_type})"


class ConditionType(models.Model):
    a_types = models.ManyToManyField(AnswerType, related_name='condition_types', related_query_name="condition_type")
    name = models.CharField(max_length=255)
    description = models.TextField(blank=True, null=True)

    def __str__(self):
        return f"{self.name}"


class Condition(models.Model):
    c_type = models.ForeignKey(ConditionType, models.DO_NOTHING)
    question = models.ForeignKey(Question, models.CASCADE)

    def __str__(self):
        return f"{self.c_type} ({self.question.id})"


class ConditionChoice(models.Model):
    condition = models.OneToOneField(Condition, models.CASCADE, primary_key=True)
    any_of = models.BooleanField(default=True)
    choices = models.ManyToManyField(QuestionChoice, related_name="+") # If you’d prefer Django not to create a backwards relation, set related_name to '+'

    class Meta:
       verbose_name = 'Choice Condition'
       verbose_name_plural = 'Choice Conditions'

    def __str__(self):
        if self.any_of:
            return f"any of {self.choices}"
        return f"all of {self.choices}"


class ConditionRange(models.Model):
    condition = models.OneToOneField(Condition, models.CASCADE, primary_key=True)
    lowest = models.DecimalField(max_digits=15, decimal_places=5, blank=True, null=True)
    highest = models.DecimalField(max_digits=15, decimal_places=5, blank=True, null=True)

    class Meta:
       verbose_name = 'Range Condition'
       verbose_name_plural = 'Range Conditions'

    def __str__(self):
        s = "["
        if self.lowest is not None:
            s += f"{remove_exponent(self.lowest)}, "
        else:
            s += "-, "
        if self.highest is not None:
            s += f"{remove_exponent(self.highest)}]"
        else:
            s += f"-]"
        return s


class ConditionDurationRange(models.Model):
    condition = models.OneToOneField(Condition, models.CASCADE, primary_key=True)
    lowest = models.TimeField(blank=True, null=True)
    highest = models.TimeField(blank=True, null=True)

    class Meta:
       verbose_name = 'Duration Range Condition'
       verbose_name_plural = 'Duration Range Conditions'

    def __str__(self):
        s = "["
        if self.lowest is not None:
            s += f"{self.lowest}, "
        else:
            s += "-, "
        if self.highest is not None:
            s += f"{self.highest}]"
        else:
            s += f"-]"
        return s


class ConditionTimeRange(models.Model):
    condition = models.OneToOneField(Condition, models.CASCADE, primary_key=True)
    lowest = models.DateTimeField(blank=True, null=True)
    highest = models.DateTimeField(blank=True, null=True)

    class Meta:
       verbose_name = 'Time Range Condition'
       verbose_name_plural = 'Time Range Conditions'

    def __str__(self):
        s = "["
        if self.lowest is not None:
            s += f"{self.lowest}, "
        else:
            s += "-, "
        if self.highest is not None:
            s += f"{self.highest}]"
        else:
            s += f"-]"
        return s
