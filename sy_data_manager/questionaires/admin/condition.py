from django.contrib import admin
from questionaires.models import ConditionBlock, Junction, Condition, ConditionChoice, ConditionType, ConditionDurationRange, ConditionRange, ConditionTimeRange

@admin.register(ConditionBlock)
class ConditionBlockAdmin(admin.ModelAdmin):
    model = ConditionBlock


@admin.register(Junction)
class JunctionAdmin(admin.ModelAdmin):
    model = Junction


@admin.register(ConditionType)
class ConditionTypeAdmin(admin.ModelAdmin):
    model = ConditionType


@admin.register(Condition)
class ConditionAdmin(admin.ModelAdmin):
    model = Condition


@admin.register(ConditionChoice)
class ConditionChoiceAdmin(admin.ModelAdmin):
    model = ConditionChoice


@admin.register(ConditionRange)
class ConditionRangeAdmin(admin.ModelAdmin):
    model = ConditionRange


@admin.register(ConditionDurationRange)
class ConditionDurationRangeAdmin(admin.ModelAdmin):
    model = ConditionDurationRange


@admin.register(ConditionTimeRange)
class ConditionTimeRangeAdmin(admin.ModelAdmin):
    model = ConditionTimeRange
