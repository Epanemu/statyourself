from django.contrib import admin
from questionaires.models import Questionaire, QuestionaireUse, Module, Question, QuestionChoice, LimitType, AnswerType

@admin.register(Questionaire)
class QuestionaireAdmin(admin.ModelAdmin):
    model = Questionaire


@admin.register(QuestionaireUse)
class QuestionaireUseAdmin(admin.ModelAdmin):
    model = QuestionaireUse


@admin.register(Module)
class ModuleAdmin(admin.ModelAdmin):
    model = Module


@admin.register(Question)
class QuestionAdmin(admin.ModelAdmin):
    model = Question


@admin.register(QuestionChoice)
class QuestionChoiceAdmin(admin.ModelAdmin):
    model = QuestionChoice


@admin.register(LimitType)
class LimitTypeAdmin(admin.ModelAdmin):
    model = LimitType


@admin.register(AnswerType)
class AnswerTypeAdmin(admin.ModelAdmin):
    model = AnswerType
