from django.contrib import admin
from questionaires.models import Limit, LimitDurationRange, LimitRange, LimitRegex, LimitTimeRange

@admin.register(Limit)
class LimitAdmin(admin.ModelAdmin):
    model = Limit


@admin.register(LimitRegex)
class LimitRegexAdmin(admin.ModelAdmin):
    model = LimitRegex


@admin.register(LimitRange)
class LimitRangeAdmin(admin.ModelAdmin):
    model = LimitRange


@admin.register(LimitDurationRange)
class LimitDurationRangeAdmin(admin.ModelAdmin):
    model = LimitDurationRange


@admin.register(LimitTimeRange)
class LimitTimeRangeAdmin(admin.ModelAdmin):
    model = LimitTimeRange
