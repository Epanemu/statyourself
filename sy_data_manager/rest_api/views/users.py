from django.contrib.auth.models import Group
from users.models import Account, AccountInfo, Settings, UITheme
from rest_framework import viewsets
from rest_framework import permissions
from rest_api.serializers import AccountSerializer, AccountInfoSerializer, SettingsSerializer, UIThemeSerializer, GroupSerializer


class AccountViewSet(viewsets.ModelViewSet):
    queryset = Account.objects.all()
    serializer_class = AccountSerializer
    # permission_classes = [permissions.IsAuthenticated]

class AccountInfoViewSet(viewsets.ModelViewSet):
    queryset = AccountInfo.objects.all()
    serializer_class = AccountInfoSerializer
    # permission_classes = [permissions.IsAuthenticated]

class SettingsViewSet(viewsets.ModelViewSet):
    queryset = Settings.objects.all()
    serializer_class = SettingsSerializer
    # permission_classes = [permissions.IsAuthenticated]

class UIThemeViewSet(viewsets.ModelViewSet):
    queryset = UITheme.objects.all()
    serializer_class = UIThemeSerializer
    # permission_classes = [permissions.IsAuthenticated]

class GroupViewSet(viewsets.ModelViewSet):
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    # permission_classes = [permissions.IsAuthenticated]
