from data_collection.models import Answer, AnswerNumber, AnswerChoice, AnswerText, AnswerDuration, AnswerTime, AnswerRange, FilledQuestionaire, FilledModule, FilledQuestion
from rest_framework import viewsets
from rest_framework import permissions
from rest_api.serializers import AnswerSerializer, AnswerNumberSerializer, AnswerChoiceSerializer, AnswerTextSerializer, AnswerDurationSerializer, AnswerTimeSerializer, AnswerRangeSerializer, FilledQuestionaireSerializer, FilledModuleSerializer, FilledQuestionSerializer

class AnswerViewSet(viewsets.ModelViewSet):
    queryset = Answer.objects.all()
    serializer_class = AnswerSerializer
    # permission_classes = [permissions.IsAuthenticated]

class AnswerNumberViewSet(viewsets.ModelViewSet):
    queryset = AnswerNumber.objects.all()
    serializer_class = AnswerNumberSerializer
    # permission_classes = [permissions.IsAuthenticated]

class AnswerChoiceViewSet(viewsets.ModelViewSet):
    queryset = AnswerChoice.objects.all()
    serializer_class = AnswerChoiceSerializer
    # permission_classes = [permissions.IsAuthenticated]

class AnswerTextViewSet(viewsets.ModelViewSet):
    queryset = AnswerText.objects.all()
    serializer_class = AnswerTextSerializer
    # permission_classes = [permissions.IsAuthenticated]

class AnswerDurationViewSet(viewsets.ModelViewSet):
    queryset = AnswerDuration.objects.all()
    serializer_class = AnswerDurationSerializer
    # permission_classes = [permissions.IsAuthenticated]

class AnswerTimeViewSet(viewsets.ModelViewSet):
    queryset = AnswerTime.objects.all()
    serializer_class = AnswerTimeSerializer
    # permission_classes = [permissions.IsAuthenticated]

class AnswerRangeViewSet(viewsets.ModelViewSet):
    queryset = AnswerRange.objects.all()
    serializer_class = AnswerRangeSerializer
    # permission_classes = [permissions.IsAuthenticated]


class FilledQuestionaireViewSet(viewsets.ModelViewSet):
    queryset = FilledQuestionaire.objects.all()
    serializer_class = FilledQuestionaireSerializer
    # permission_classes = [permissions.IsAuthenticated]

class FilledModuleViewSet(viewsets.ModelViewSet):
    queryset = FilledModule.objects.all()
    serializer_class = FilledModuleSerializer
    # permission_classes = [permissions.IsAuthenticated]

class FilledQuestionViewSet(viewsets.ModelViewSet):
    queryset = FilledQuestion.objects.all()
    serializer_class = FilledQuestionSerializer
    # permission_classes = [permissions.IsAuthenticated]
