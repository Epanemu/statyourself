from questionaires.models import ConditionBlock, Junction, ConditionType, Condition, ConditionChoice, ConditionRange, ConditionDurationRange, ConditionTimeRange, Limit, LimitRegex, LimitRange, LimitDurationRange, LimitTimeRange, AnswerType, LimitType, Questionaire, QuestionaireUse, Module, Question, QuestionChoice
from rest_framework import viewsets
from rest_framework import permissions
from rest_api.serializers import ConditionBlockSerializer, JunctionSerializer, ConditionTypeSerializer, ConditionSerializer, ConditionChoiceSerializer, ConditionRangeSerializer, ConditionDurationRangeSerializer, ConditionTimeRangeSerializer, LimitSerializer, LimitRegexSerializer, LimitRangeSerializer, LimitDurationRangeSerializer, LimitTimeRangeSerializer, AnswerTypeSerializer, LimitTypeSerializer, QuestionaireSerializer, QuestionaireUseSerializer, ModuleSerializer, QuestionSerializer, QuestionChoiceSerializer

from users.models import Account
from rest_framework.decorators import action
from django.http import JsonResponse
import json


class ConditionBlockViewSet(viewsets.ModelViewSet):
    queryset = ConditionBlock.objects.all()
    serializer_class = ConditionBlockSerializer
    # permission_classes = [permissions.IsAuthenticated]

class JunctionViewSet(viewsets.ModelViewSet):
    queryset = Junction.objects.all()
    serializer_class = JunctionSerializer
    # permission_classes = [permissions.IsAuthenticated]

class ConditionTypeViewSet(viewsets.ModelViewSet):
    queryset = ConditionType.objects.all()
    serializer_class = ConditionTypeSerializer
    # permission_classes = [permissions.IsAuthenticated]

class ConditionViewSet(viewsets.ModelViewSet):
    queryset = Condition.objects.all()
    serializer_class = ConditionSerializer
    # permission_classes = [permissions.IsAuthenticated]

class ConditionChoiceViewSet(viewsets.ModelViewSet):
    queryset = ConditionChoice.objects.all()
    serializer_class = ConditionChoiceSerializer
    # permission_classes = [permissions.IsAuthenticated]

class ConditionRangeViewSet(viewsets.ModelViewSet):
    queryset = ConditionRange.objects.all()
    serializer_class = ConditionRangeSerializer
    # permission_classes = [permissions.IsAuthenticated]

class ConditionDurationRangeViewSet(viewsets.ModelViewSet):
    queryset = ConditionDurationRange.objects.all()
    serializer_class = ConditionDurationRangeSerializer
    # permission_classes = [permissions.IsAuthenticated]

class ConditionTimeRangeViewSet(viewsets.ModelViewSet):
    queryset = ConditionTimeRange.objects.all()
    serializer_class = ConditionTimeRangeSerializer
    # permission_classes = [permissions.IsAuthenticated]


class LimitViewSet(viewsets.ModelViewSet):
    queryset = Limit.objects.all()
    serializer_class = LimitSerializer
    # permission_classes = [permissions.IsAuthenticated]

class LimitRegexViewSet(viewsets.ModelViewSet):
    queryset = LimitRegex.objects.all()
    serializer_class = LimitRegexSerializer
    # permission_classes = [permissions.IsAuthenticated]

class LimitRangeViewSet(viewsets.ModelViewSet):
    queryset = LimitRange.objects.all()
    serializer_class = LimitRangeSerializer
    # permission_classes = [permissions.IsAuthenticated]

class LimitDurationRangeViewSet(viewsets.ModelViewSet):
    queryset = LimitDurationRange.objects.all()
    serializer_class = LimitDurationRangeSerializer
    # permission_classes = [permissions.IsAuthenticated]

class LimitTimeRangeViewSet(viewsets.ModelViewSet):
    queryset = LimitTimeRange.objects.all()
    serializer_class = LimitTimeRangeSerializer
    # permission_classes = [permissions.IsAuthenticated]


class AnswerTypeViewSet(viewsets.ModelViewSet):
    queryset = AnswerType.objects.all()
    serializer_class = AnswerTypeSerializer
    # permission_classes = [permissions.IsAuthenticated]

class LimitTypeViewSet(viewsets.ModelViewSet):
    queryset = LimitType.objects.all()
    serializer_class = LimitTypeSerializer
    # permission_classes = [permissions.IsAuthenticated]

class QuestionaireViewSet(viewsets.ModelViewSet):
    queryset = Questionaire.objects.all()
    serializer_class = QuestionaireSerializer
    # permission_classes = [permissions.IsAuthenticated]

    @action(detail=False, methods=['POST'])
    def extended(self, request):
        data_string = request.body.decode('utf-8')
        data = json.loads(data_string)

        creator = Account.objects.get(username='epanemu')
        questionnaire = Questionaire(creator=creator, name=data["name"])
        questionnaire.save()

        i = 0
        for m in data["modules"]:
            module = Module(name=m["name"], questionnaire=questionnaire, order=i)
            module.save()
            # questionnaire.module_set.add(module, bulk=False)
            j = 0
            for q in m["questions"]:
                a_type = AnswerType.objects.get(name=q["answerType"])
                question = Question(question=q["question"], order=j, importance=0, module=module, a_type=a_type)
                question.save()
                if "choice" in  a_type.name:
                    for c in q["choices"]:
                        choice = QuestionChoice(question=question, name=c)
                        choice.save()

                j += 1
            i += 1
        questionnaire.save()

        return JsonResponse(data, safe=False)

class QuestionaireUseViewSet(viewsets.ModelViewSet):
    queryset = QuestionaireUse.objects.all()
    serializer_class = QuestionaireUseSerializer
    # permission_classes = [permissions.IsAuthenticated]

class ModuleViewSet(viewsets.ModelViewSet):
    queryset = Module.objects.all()
    serializer_class = ModuleSerializer
    # permission_classes = [permissions.IsAuthenticated]

class QuestionViewSet(viewsets.ModelViewSet):
    queryset = Question.objects.all()
    serializer_class = QuestionSerializer
    # permission_classes = [permissions.IsAuthenticated]

class QuestionChoiceViewSet(viewsets.ModelViewSet):
    queryset = QuestionChoice.objects.all()
    serializer_class = QuestionChoiceSerializer
    # permission_classes = [permissions.IsAuthenticated]
