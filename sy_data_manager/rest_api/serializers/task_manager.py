from task_manager.models import Recurrence, Theme, Subject, Goal, Task, Todo
from rest_framework import serializers

class RecurrenceSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Recurrence
        fields = ['url', 'start_date', 'period_time', 'period_month', 'period_year', 'total_recurrences', 'until_date']

class ThemeSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Theme
        fields = ['url', 'parent', 'account', 'name', 'description', 'date_created', 'active']

class SubjectSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Subject
        fields = ['url', 'theme', 'name', 'description', 'date_created', 'notifications', 'active']

class GoalSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Goal
        fields = ['url', 'start', 'total', 'step']

class TaskSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Task
        fields = ['url', 'recurrence', 'goal', 'name', 'description', 'date_created', 'notifications', 'subjects', 'active']

class TodoSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Todo
        fields = ['url', 'task', 'done', 'due_date', 'amount', 'active']