from django.contrib.auth.models import Group
from users.models import Account, AccountInfo, Settings, UITheme
from rest_framework import serializers


class AccountSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Account
        fields = ['url', 'username', 'email', 'groups', 'display_name', 'questionaires']

class AccountInfoSerializer(serializers.HyperlinkedModelSerializer):
    account = serializers.HyperlinkedIdentityField(view_name='account-detail')

    class Meta:
        model = AccountInfo
        fields = ['url', 'account', 'date_created', 'last_login']

class SettingsSerializer(serializers.HyperlinkedModelSerializer):
    account = serializers.HyperlinkedIdentityField(view_name='account-detail')

    class Meta:
        model = Settings
        fields = ['url', 'account', 'theme', 'app_notifications', 'desktop_notifications']

class UIThemeSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = UITheme
        fields = ['url', 'name', 'primary_color', 'secondary_color', 'text_color', 'font']

class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ['url', 'name']