from questionaires.models import ConditionBlock, Junction, ConditionType, Condition, ConditionChoice, ConditionRange, ConditionDurationRange, ConditionTimeRange, Limit, LimitRegex, LimitRange, LimitDurationRange, LimitTimeRange, AnswerType, LimitType, Questionaire, QuestionaireUse, Module, Question, QuestionChoice
from rest_framework import serializers

class ConditionBlockSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = ConditionBlock
        fields = ['url', 'id', 'negation', 'part_of', 'condition']

class JunctionSerializer(serializers.HyperlinkedModelSerializer):
    c_block = serializers.HyperlinkedIdentityField(view_name='condition_block-detail')

    class Meta:
        model = Junction
        fields = ['url', 'id', 'c_block', 'c_type']

class ConditionTypeSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = ConditionType
        fields = ['url', 'id', 'a_types', 'name', 'description']

class ConditionSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Condition
        fields = ['url', 'id', 'c_type', 'question']

class ConditionChoiceSerializer(serializers.HyperlinkedModelSerializer):
    condition = serializers.HyperlinkedIdentityField(view_name='condition-detail')

    class Meta:
        model = ConditionChoice
        fields = ['url', 'id', 'condition', 'any_of', 'choices']

class ConditionRangeSerializer(serializers.HyperlinkedModelSerializer):
    condition = serializers.HyperlinkedIdentityField(view_name='condition-detail')

    class Meta:
        model = ConditionRange
        fields = ['url', 'id', 'condition', 'lowest', 'highest']

class ConditionDurationRangeSerializer(serializers.HyperlinkedModelSerializer):
    condition = serializers.HyperlinkedIdentityField(view_name='condition-detail')

    class Meta:
        model = ConditionDurationRange
        fields = ['url', 'id', 'condition', 'lowest', 'highest']

class ConditionTimeRangeSerializer(serializers.HyperlinkedModelSerializer):
    condition = serializers.HyperlinkedIdentityField(view_name='condition-detail')

    class Meta:
        model = ConditionTimeRange
        fields = ['url', 'id', 'condition', 'lowest', 'highest']


class LimitSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Limit
        fields = ['url', 'id', 'question', 'inverted', 'limit_type']

class LimitRegexSerializer(serializers.HyperlinkedModelSerializer):
    q_limit = serializers.HyperlinkedIdentityField(view_name='limit-detail')

    class Meta:
        model = LimitRegex
        fields = ['url', 'id', 'q_limit', 'regex']

class LimitRangeSerializer(serializers.HyperlinkedModelSerializer):
    q_limit = serializers.HyperlinkedIdentityField(view_name='limit-detail')

    class Meta:
        model = LimitRange
        fields = ['url', 'id', 'q_limit', 'lowest', 'highest', 'step']

class LimitDurationRangeSerializer(serializers.HyperlinkedModelSerializer):
    q_limit = serializers.HyperlinkedIdentityField(view_name='limit-detail')

    class Meta:
        model = LimitDurationRange
        fields = ['url', 'id', 'q_limit', 'lowest', 'highest', 'step']

class LimitTimeRangeSerializer(serializers.HyperlinkedModelSerializer):
    q_limit = serializers.HyperlinkedIdentityField(view_name='limit-detail')

    class Meta:
        model = LimitTimeRange
        fields = ['url', 'id', 'q_limit', 'lowest', 'highest']


class AnswerTypeSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = AnswerType
        fields = ['url', 'id', 'name', 'verbose_name', 'description']

class LimitTypeSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = LimitType
        fields = ['url', 'id', 'a_types', 'name', 'description']

class QuestionaireSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Questionaire
        fields = ['url', 'id', 'creator', 'name', 'description', 'date_created', 'active']

class QuestionaireUseSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = QuestionaireUse
        fields = ['url', 'id', 'account', 'questionnaire', 'recurrence', 'date_started', 'active', 'notifications']

class ModuleSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Module
        fields = ['url', 'id', 'name', 'questionnaire', 'parent_module', 'subject', 'c_block', 'active', 'order']

class QuestionSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Question
        fields = ['url', 'id', 'module', 'c_block', 'a_type', 'active', 'order', 'question', 'importance']

class QuestionChoiceSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = QuestionChoice
        fields = ['url', 'id', 'question', 'name']
