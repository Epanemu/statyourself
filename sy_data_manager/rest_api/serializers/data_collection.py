from data_collection.models import Answer, AnswerNumber, AnswerChoice, AnswerText, AnswerDuration, AnswerTime, AnswerRange, FilledQuestionaire, FilledModule, FilledQuestion
from rest_framework import serializers


class AnswerSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Answer
        fields = ['url', 'f_question', 'answer_date']

class AnswerNumberSerializer(serializers.HyperlinkedModelSerializer):
    answer = serializers.HyperlinkedIdentityField(view_name='answer-detail')

    class Meta:
        model = AnswerNumber
        fields = ['url', 'answer', 'value']

class AnswerChoiceSerializer(serializers.HyperlinkedModelSerializer):
    answer = serializers.HyperlinkedIdentityField(view_name='answer-detail')

    class Meta:
        model = AnswerChoice
        fields = ['url', 'answer', 'choice']

class AnswerTextSerializer(serializers.HyperlinkedModelSerializer):
    answer = serializers.HyperlinkedIdentityField(view_name='answer-detail')

    class Meta:
        model = AnswerText
        fields = ['url', 'answer', 'value']

class AnswerDurationSerializer(serializers.HyperlinkedModelSerializer):
    answer = serializers.HyperlinkedIdentityField(view_name='answer-detail')

    class Meta:
        model = AnswerDuration
        fields = ['url', 'answer', 'value']

class AnswerTimeSerializer(serializers.HyperlinkedModelSerializer):
    answer = serializers.HyperlinkedIdentityField(view_name='answer-detail')

    class Meta:
        model = AnswerTime
        fields = ['url', 'answer', 'value']

class AnswerRangeSerializer(serializers.HyperlinkedModelSerializer):
    answer = serializers.HyperlinkedIdentityField(view_name='answer-detail')

    class Meta:
        model = AnswerRange
        fields = ['url', 'answer', 'lowest', 'highest']


class FilledQuestionaireSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = FilledQuestionaire
        fields = ['url', 'questionaire', 'account', 'expected_fill', 'filled_in', 'last_edit']

class FilledModuleSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = FilledModule
        fields = ['url', 'module', 'f_questionaire', 'shown', 'filled_in', 'time_taken']

class FilledQuestionSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = FilledQuestion
        fields = ['url', 'question', 'f_module', 'shown', 'filled_in', 'last_edit', 'edits']
