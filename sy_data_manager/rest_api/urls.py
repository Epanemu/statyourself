from django.urls import include, path
from rest_framework import routers
from rest_api import views

router = routers.DefaultRouter()
router.register(r'accounts', views.AccountViewSet)
router.register(r'accounts_info', views.AccountInfoViewSet)
router.register(r'settings', views.SettingsViewSet)
router.register(r'ui_themes', views.UIThemeViewSet)
router.register(r'groups', views.GroupViewSet)

router.register(r'recurrences', views.RecurrenceViewSet)
router.register(r'themes', views.ThemeViewSet)
router.register(r'subjects', views.SubjectViewSet)
router.register(r'goals', views.GoalViewSet)
router.register(r'tasks', views.TaskViewSet)
router.register(r'todos', views.TodoViewSet)

router.register(r'condition_block', views.ConditionBlockViewSet)
router.register(r'junction', views.JunctionViewSet)
router.register(r'condition_type', views.ConditionTypeViewSet)
router.register(r'condition', views.ConditionViewSet)
router.register(r'condition_choice', views.ConditionChoiceViewSet)
router.register(r'condition_range', views.ConditionRangeViewSet)
router.register(r'condition_duration_range', views.ConditionDurationRangeViewSet)
router.register(r'condition_time_range', views.ConditionTimeRangeViewSet)

router.register(r'limit', views.LimitViewSet)
router.register(r'limit_regex', views.LimitRegexViewSet)
router.register(r'limit_range', views.LimitRangeViewSet)
router.register(r'limit_duration_range', views.LimitDurationRangeViewSet)
router.register(r'limit_time_range', views.LimitTimeRangeViewSet)

router.register(r'answer_type', views.AnswerTypeViewSet)
router.register(r'limit_type', views.LimitTypeViewSet)
router.register(r'questionnaire', views.QuestionaireViewSet)
router.register(r'questionnaire_use', views.QuestionaireUseViewSet)
router.register(r'module', views.ModuleViewSet)
router.register(r'question', views.QuestionViewSet)
router.register(r'question_choice', views.QuestionChoiceViewSet)

router.register(r'answer', views.AnswerViewSet)
router.register(r'answer_number', views.AnswerNumberViewSet)
router.register(r'answer_choice', views.AnswerChoiceViewSet)
router.register(r'answer_text', views.AnswerTextViewSet)
router.register(r'answer_duration', views.AnswerDurationViewSet)
router.register(r'answer_time', views.AnswerTimeViewSet)
router.register(r'answer_range', views.AnswerRangeViewSet)

router.register(r'filled_questionaire', views.FilledQuestionaireViewSet)
router.register(r'filled_module', views.FilledModuleViewSet)
router.register(r'filled_question', views.FilledQuestionViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]