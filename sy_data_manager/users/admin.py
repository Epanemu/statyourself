from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin

from .forms import AccountCreationForm, AccountChangeForm
from .models import Account, AccountInfo, Settings, UITheme

@admin.register(Account)
class AccountAdmin(UserAdmin):
    add_form = AccountCreationForm
    form = AccountChangeForm
    model = Account
    list_display = ['username', 'email',]

    fieldsets = (UserAdmin.fieldsets[0],) + (
        (None, {'fields': ('display_name',)}),
    ) + UserAdmin.fieldsets[1:]


@admin.register(AccountInfo)
class AccountInfoAdmin(admin.ModelAdmin):
    model = AccountInfo


@admin.register(Settings)
class SettingsAdmin(admin.ModelAdmin):
    model = Settings


@admin.register(UITheme)
class UIThemeAdmin(admin.ModelAdmin):
    model = UITheme
