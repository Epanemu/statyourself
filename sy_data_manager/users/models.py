from django.contrib.auth.models import AbstractUser
from django.db import models

class Account(AbstractUser):
    display_name = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        verbose_name = 'Account'

    def __unicode__(self):
        return self.username


class AccountInfo(models.Model):
    account = models.OneToOneField(Account, models.CASCADE, primary_key=True)
    date_created = models.DateTimeField()
    last_login = models.DateTimeField(blank=True, null=True)

    class Meta:
        verbose_name = 'Account info'
        verbose_name_plural = 'Accounts info'

    def __unicode__(self):
        return self.account + ' info'


class UITheme(models.Model):
    name = models.CharField(max_length=45, blank=True, null=True)
    primary_color = models.CharField(max_length=20, blank=True, null=True)
    secondary_color = models.CharField(max_length=20, blank=True, null=True)
    text_color = models.CharField(max_length=20, blank=True, null=True)
    font = models.CharField(max_length=45, blank=True, null=True)

    def __unicode__(self):
        return self.name


class Settings(models.Model):
    account = models.OneToOneField(Account, models.CASCADE, primary_key=True)
    theme = models.ForeignKey(UITheme, models.DO_NOTHING, blank=True, null=True)
    app_notifications = models.IntegerField(blank=True, null=True)
    desktop_notifications = models.IntegerField(blank=True, null=True)

    class Meta:
        verbose_name = 'Settings'
        verbose_name_plural = 'Settings'

    def __unicode__(self):
        return self.account + ' settings'
