from django.db import models
from users.models import Account
from utils import remove_exponent

from enum import Enum

class Notifications(Enum):
    ALL = 1
    MINIMAL = 2
    ONLY_SPECIFIED = 3
    NONE = 4

class Recurrence(models.Model):
    start_date = models.DateTimeField()
    period_time = models.TimeField(blank=True, null=True)
    period_month = models.IntegerField(blank=True, null=True)
    period_year = models.IntegerField(blank=True, null=True)
    total_recurrences = models.IntegerField(blank=True, null=True)
    until_date = models.DateTimeField(blank=True, null=True)

    def __unicode__(self):
        # TODO: rework this to show proper recurrence
        return self.id + ' recurrence'


class Theme(models.Model):
    parent = models.ForeignKey('self', models.SET_NULL, blank=True, null=True)
    account = models.ForeignKey(Account, models.CASCADE)
    name = models.CharField(max_length=255)
    description = models.TextField(blank=True, null=True)
    date_created = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    active = models.BooleanField(default=True)

    class Meta:
        unique_together = (('name', 'account'),)

    def __str__(self):
        return f"{self.name}"


class Subject(models.Model):
    theme = models.ForeignKey(Theme, models.CASCADE)
    name = models.CharField(max_length=255)
    description = models.TextField(blank=True, null=True)
    date_created = models.DateTimeField(auto_now_add=True)
    notifications = models.IntegerField(choices=[(tag.value, tag.name) for tag in Notifications], default=Notifications.MINIMAL.value)
    active = models.BooleanField(default=True)

    class Meta:
        unique_together = (('name', 'theme'),)

    def __str__(self):
        return f"{self.name} ({self.theme})"


class Goal(models.Model):
    start = models.DecimalField(max_digits=15, decimal_places=5)
    total = models.DecimalField(max_digits=15, decimal_places=5)
    step = models.DecimalField(max_digits=15, decimal_places=5, blank=True, null=True)

    def __str__(self):
        if self.step is not None:
            return f"{remove_exponent(self.start)} ➞ {remove_exponent(self.total)} ({remove_exponent(self.step)})"
        return f"{remove_exponent(self.start)} ➞ {remove_exponent(self.total)}"


class Task(models.Model):
    recurrence = models.ForeignKey(Recurrence, models.SET_NULL, blank=True, null=True)
    goal = models.ForeignKey(Goal, models.SET_NULL, blank=True, null=True)
    name = models.CharField(max_length=255)
    description = models.TextField(blank=True, null=True)
    date_created = models.DateTimeField(auto_now_add=True)
    notifications = models.IntegerField(choices=[(tag.value, tag.name) for tag in Notifications], default=Notifications.MINIMAL.value)
    subjects = models.ManyToManyField(Subject, related_name="tasks", related_query_name="task")
    active = models.BooleanField(default=True)

    def __str__(self):
        if self.goal is not None:
            return f"{self.name} ({self.goal})"
        return f"{self.name}"


class Todo(models.Model):
    task = models.ForeignKey(Task, models.CASCADE)
    done = models.BooleanField(default=False)
    due_date = models.DateTimeField(blank=True, null=True)
    amount = models.DecimalField(max_digits=15, decimal_places=5, blank=True, null=True)
    active = models.BooleanField(default=True)

    def __str__(self):
        if self.done:
            return f"{self.task} ✓"
        return f"{self.task} ✗"
