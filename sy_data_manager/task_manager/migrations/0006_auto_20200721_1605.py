# Generated by Django 3.0.8 on 2020-07-21 16:05

from django.db import migrations, models
import task_manager.models


class Migration(migrations.Migration):

    dependencies = [
        ('task_manager', '0005_auto_20200721_1602'),
    ]

    operations = [
        migrations.AlterField(
            model_name='subject',
            name='notifications',
            field=models.IntegerField(choices=[(1, task_manager.models.Notifications['ALL']), (2, task_manager.models.Notifications['MINIMAL']), (3, task_manager.models.Notifications['ONLY_SPECIFIED']), (4, task_manager.models.Notifications['NONE'])], default=2),
        ),
        migrations.AlterField(
            model_name='task',
            name='notifications',
            field=models.IntegerField(choices=[(1, task_manager.models.Notifications['ALL']), (2, task_manager.models.Notifications['MINIMAL']), (3, task_manager.models.Notifications['ONLY_SPECIFIED']), (4, task_manager.models.Notifications['NONE'])], default=2),
        ),
    ]
