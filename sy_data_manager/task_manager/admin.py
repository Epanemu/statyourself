from django.contrib import admin
from .models import Recurrence, Theme, Subject, Goal, Task, Todo

from django.db import models
from django.forms import Textarea

@admin.register(Recurrence)
class RecurrenceAdmin(admin.ModelAdmin):
    model = Recurrence


class SubThemeInline(admin.TabularInline):
    model = Theme
    verbose_name = 'Subtheme'
    verbose_name_plural = 'Subthemes'
    classes = ('collapse',)
    fields = ('name', 'description')

    formfield_overrides = {
        models.TextField: {'widget': Textarea(attrs={'rows': 1, 'cols': 40})},
    }


class ParentListFilter(admin.SimpleListFilter):
    title = 'Parent Theme'

    parameter_name = 'parent'

    def lookups(self, request, model_admin):
        q_set = Theme.objects.exclude(parent__isnull=True)
        parents = [('_no_parent', 'No Parent')]
        for theme in q_set:
            if (theme.parent.id, theme.parent.name) not in parents:
                parents.append((theme.parent.id, theme.parent.name))
        return parents

    def choices(self, cl):
        for lookup, title in self.lookup_choices:
            yield {
                'selected': self.value() == lookup,
                'query_string': cl.get_query_string({
                self.parameter_name: lookup,
            }, []),
        'display': title,
        }

    def queryset(self, request, queryset):
        if self.value() is None:
            return queryset
        elif self.value() == '_no_parent':
            return queryset.filter(parent__isnull=True)
        return queryset.filter(parent__id = self.value())


@admin.register(Theme)
class ThemeAdmin(admin.ModelAdmin):
    model = Theme

    fields = ('name', 'description', 'parent')
    list_display = ('name', 'parent', 'date_created')
    list_filter = (ParentListFilter,)
    inlines = (SubThemeInline,)

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(account=request.user)

    def save_model(self, request, obj, form, change):
        if obj.account_id is None:
            obj.account_id = request.user.id
        super().save_model(request, obj, form, change)

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for instance in instances:
            instance.account = instance.parent.account
            instance.save()
        formset.save_m2m()
        # super().save_formset(request, form, formset, change)


class ThemeFilter(admin.SimpleListFilter):
    title = 'Theme'

    parameter_name = 'theme'

    def lookups(self, request, model_admin):
        q_set = Theme.objects.filter(account=request.user)
        themes = []
        for theme in q_set:
            if (theme.id, theme.name) not in themes:
                themes.append((theme.id, theme.name))
        return themes


    def queryset(self, request, queryset):
        if self.value() is None:
            return queryset
        return queryset.filter(theme__id = self.value())


@admin.register(Subject)
class SubjectAdmin(admin.ModelAdmin):
    model = Subject

    fields = ('name', 'description', 'theme', 'notifications')
    list_display = ('name', 'theme', 'date_created')
    list_filter = (ThemeFilter,)

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "theme":
            kwargs["queryset"] = Theme.objects.filter(account=request.user)
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(theme__account=request.user)


@admin.register(Goal)
class GoalAdmin(admin.ModelAdmin):
    model = Goal

    fields = ('start', 'total', 'step')
    list_display = ('start', 'total', 'step')

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs
        # this is supposed to be returning only owners results. I stopped here, this probably doesn't work so it is commented out
        # return qs.filter(task_set__account=request.user).prefetch_related(Prefetch('account', queryset=Task.objects.filter(Goal.account)))
        return qs

@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    model = Task


@admin.register(Todo)
class TodoAdmin(admin.ModelAdmin):
    model = Todo
