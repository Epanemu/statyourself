from django.contrib import admin
from data_visualization.models import FilterType, Filter, FilterChoice, FilterRange, FilterDurationRange, FilterTimeRange, FilterFilled

@admin.register(FilterType)
class FilterTypeAdmin(admin.ModelAdmin):
    model = FilterType


@admin.register(Filter)
class FilterAdmin(admin.ModelAdmin):
    model = Filter


@admin.register(FilterChoice)
class FilterChoiceAdmin(admin.ModelAdmin):
    model = FilterChoice


@admin.register(FilterRange)
class FilterRangeAdmin(admin.ModelAdmin):
    model = FilterRange


@admin.register(FilterDurationRange)
class FilterDurationRangeAdmin(admin.ModelAdmin):
    model = FilterDurationRange


@admin.register(FilterTimeRange)
class FilterTimeRangeAdmin(admin.ModelAdmin):
    model = FilterTimeRange


@admin.register(FilterFilled)
class FilterFilledAdmin(admin.ModelAdmin):
    model = FilterFilled
