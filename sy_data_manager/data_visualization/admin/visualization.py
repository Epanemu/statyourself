from django.contrib import admin
from data_visualization.models import VisualizationType, Visualization, Comparison, DataView

@admin.register(VisualizationType)
class VisualizationTypeAdmin(admin.ModelAdmin):
    model = VisualizationType


@admin.register(Visualization)
class VisualizationAdmin(admin.ModelAdmin):
    model = Visualization


@admin.register(Comparison)
class ComparisonAdmin(admin.ModelAdmin):
    model = Comparison


@admin.register(DataView)
class DataViewAdmin(admin.ModelAdmin):
    model = DataView
