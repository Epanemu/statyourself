# Generated by Django 3.0.8 on 2020-07-18 14:38

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('questionaires', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Comparison',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('date_created', models.DateTimeField(blank=True, null=True)),
                ('account', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'unique_together': {('name', 'account')},
            },
        ),
        migrations.CreateModel(
            name='DataView',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('comparison', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='data_visualization.Comparison')),
                ('question', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='questionaires.Question')),
            ],
        ),
        migrations.CreateModel(
            name='Filter',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('data_view', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='data_visualization.DataView')),
            ],
        ),
        migrations.CreateModel(
            name='FilterDurationRange',
            fields=[
                ('filter', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, primary_key=True, serialize=False, to='data_visualization.Filter')),
                ('lowest', models.TimeField(blank=True, null=True)),
                ('highest', models.TimeField(blank=True, null=True)),
            ],
            options={
                'verbose_name': 'Duration Range Filter',
                'verbose_name_plural': 'Duration Range Filters',
            },
        ),
        migrations.CreateModel(
            name='FilterFilled',
            fields=[
                ('filter', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, primary_key=True, serialize=False, to='data_visualization.Filter')),
                ('shown', models.IntegerField(blank=True, null=True)),
                ('filled', models.IntegerField(blank=True, null=True)),
            ],
            options={
                'verbose_name': 'Filled Filter',
                'verbose_name_plural': 'Filled Filters',
            },
        ),
        migrations.CreateModel(
            name='FilterRange',
            fields=[
                ('filter', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, primary_key=True, serialize=False, to='data_visualization.Filter')),
                ('lowest', models.DecimalField(blank=True, decimal_places=5, max_digits=15, null=True)),
                ('highest', models.DecimalField(blank=True, decimal_places=5, max_digits=15, null=True)),
            ],
            options={
                'verbose_name': 'Range Filter',
                'verbose_name_plural': 'Range Filters',
            },
        ),
        migrations.CreateModel(
            name='FilterTimeRange',
            fields=[
                ('filter', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, primary_key=True, serialize=False, to='data_visualization.Filter')),
                ('lowest', models.DateTimeField(blank=True, null=True)),
                ('highest', models.DateTimeField(blank=True, null=True)),
            ],
            options={
                'verbose_name': 'Time Range Filter',
                'verbose_name_plural': 'Time Range Filters',
            },
        ),
        migrations.CreateModel(
            name='VisualizationType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('answer_types', models.ManyToManyField(related_name='visualization_types', related_query_name='visualization_type', to='questionaires.AnswerType')),
            ],
        ),
        migrations.CreateModel(
            name='FilterType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=45, null=True)),
                ('a_type', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='questionaires.AnswerType')),
            ],
        ),
        migrations.AddField(
            model_name='filter',
            name='f_type',
            field=models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='data_visualization.FilterType'),
        ),
        migrations.AddField(
            model_name='filter',
            name='question',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='questionaires.Question'),
        ),
        migrations.AddField(
            model_name='dataview',
            name='v_type',
            field=models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='data_visualization.VisualizationType'),
        ),
        migrations.CreateModel(
            name='Visualization',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=255, null=True)),
                ('account', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to=settings.AUTH_USER_MODEL)),
                ('question', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='questionaires.Question')),
                ('v_type', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='data_visualization.VisualizationType')),
            ],
            options={
                'unique_together': {('name', 'account')},
            },
        ),
        migrations.CreateModel(
            name='FilterChoice',
            fields=[
                ('filter', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, primary_key=True, serialize=False, to='data_visualization.Filter')),
                ('any_of', models.BooleanField(default=True)),
                ('choices', models.ManyToManyField(related_name='_filterchoice_choices_+', to='questionaires.QuestionChoice')),
            ],
            options={
                'verbose_name': 'Choice Filter',
                'verbose_name_plural': 'Choice Filters',
            },
        ),
    ]
