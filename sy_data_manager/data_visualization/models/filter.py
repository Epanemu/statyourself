from django.db import models
from .visualization import DataView
from questionaires.models import AnswerType, Question, QuestionChoice
from utils import remove_exponent

class FilterType(models.Model):
    a_type = models.ForeignKey(AnswerType, models.DO_NOTHING)
    name = models.CharField(max_length=45, blank=True, null=True)

    def __str__(self):
        return f"{self.name}"


class Filter(models.Model):
    data_view = models.ForeignKey(DataView, models.CASCADE)
    question = models.ForeignKey(Question, models.CASCADE)
    f_type = models.ForeignKey(FilterType, models.DO_NOTHING)

    def __str__(self):
        return f"{self.f_type} {self.data_view}"


class FilterChoice(models.Model):
    filter = models.OneToOneField(Filter, models.CASCADE, primary_key=True)
    any_of = models.BooleanField(default=True)
    choices = models.ManyToManyField(QuestionChoice, related_name="+") # If you’d prefer Django not to create a backwards relation, set related_name to '+'

    class Meta:
        verbose_name = "Choice Filter"
        verbose_name_plural = "Choice Filters"

    def __str__(self):
        if self.any_of:
            return f"any of {self.choices}"
        return f"all of {self.choices}"


class FilterRange(models.Model):
    filter = models.OneToOneField(Filter, models.CASCADE, primary_key=True)
    lowest = models.DecimalField(max_digits=15, decimal_places=5, blank=True, null=True)
    highest = models.DecimalField(max_digits=15, decimal_places=5, blank=True, null=True)

    class Meta:
        verbose_name = "Range Filter"
        verbose_name_plural = "Range Filters"

    def __str__(self):
        s = "["
        if self.lowest is not None:
            s += f"{remove_exponent(self.lowest)}, "
        else:
            s += "-, "
        if self.highest is not None:
            s += f"{remove_exponent(self.highest)}]"
        else:
            s += f"-]"
        return s


class FilterDurationRange(models.Model):
    filter = models.OneToOneField(Filter, models.CASCADE, primary_key=True)
    lowest = models.TimeField(blank=True, null=True)
    highest = models.TimeField(blank=True, null=True)

    class Meta:
        verbose_name = "Duration Range Filter"
        verbose_name_plural = "Duration Range Filters"

    def __str__(self):
        s = "["
        if self.lowest is not None:
            s += f"{self.lowest}, "
        else:
            s += "-, "
        if self.highest is not None:
            s += f"{self.highest}]"
        else:
            s += f"-]"
        return s


class FilterTimeRange(models.Model):
    filter = models.OneToOneField(Filter, models.CASCADE, primary_key=True)
    lowest = models.DateTimeField(blank=True, null=True)
    highest = models.DateTimeField(blank=True, null=True)

    class Meta:
        verbose_name = "Time Range Filter"
        verbose_name_plural = "Time Range Filters"

    def __str__(self):
        s = "["
        if self.lowest is not None:
            s += f"{self.lowest}, "
        else:
            s += "-, "
        if self.highest is not None:
            s += f"{self.highest}]"
        else:
            s += f"-]"
        return s


class FilterFilled(models.Model):
    filter = models.OneToOneField(Filter, models.CASCADE, primary_key=True)
    shown = models.IntegerField(blank=True, null=True)
    filled = models.IntegerField(blank=True, null=True)

    class Meta:
        verbose_name = "Filled Filter"
        verbose_name_plural = "Filled Filters"

    def __str__(self):
        s = ""
        if self.shown:
            s += "was shown"
            if self.filled:
                s += " and was filled"
        elif self.filled:
            s += "was filled"
        else:
            return f"{self.filter}"
        return s
