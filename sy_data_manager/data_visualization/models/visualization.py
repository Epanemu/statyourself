from django.db import models
from questionaires.models import AnswerType, Question
from users.models import Account

class VisualizationType(models.Model):
    name = models.CharField(max_length=255)
    answer_types = models.ManyToManyField(AnswerType, related_name="visualization_types", related_query_name="visualization_type")

    def __str__(self):
        return f"{self.name}"


class Visualization(models.Model):
    question = models.ForeignKey(Question, models.DO_NOTHING)
    account = models.ForeignKey(Account, models.DO_NOTHING)
    v_type = models.ForeignKey(VisualizationType, models.DO_NOTHING)
    name = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        unique_together = (('name', 'account'),)

    def __str__(self):
        return f"{self.name} ({self.v_type})"


class Comparison(models.Model):
    account = models.ForeignKey(Account, models.DO_NOTHING)
    name = models.CharField(max_length=255)
    date_created = models.DateTimeField(blank=True, null=True)

    class Meta:
        unique_together = (('name', 'account'),)

    def __str__(self):
        return f"{self.name}"


class DataView(models.Model):
    comparison = models.ForeignKey(Comparison, models.CASCADE)
    v_type = models.ForeignKey(VisualizationType, models.DO_NOTHING)
    question = models.ForeignKey(Question, models.DO_NOTHING)

    def __str__(self):
        return f"{self.comparison} ({self.v_type})"
