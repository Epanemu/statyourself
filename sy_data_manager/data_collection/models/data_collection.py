from django.db import models
from questionaires.models import Questionaire, Module, Question
from users.models import Account

class FilledQuestionaire(models.Model):
    questionaire = models.ForeignKey(Questionaire, models.DO_NOTHING)
    account = models.ForeignKey(Account, models.CASCADE)
    expected_fill = models.DateTimeField(blank=True, null=True)
    filled_in = models.DateTimeField(blank=True, null=True)
    last_edit = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return f"{self.questionaire} {self.account} {self.expected_fill}"


class FilledModule(models.Model):
    module = models.ForeignKey(Module, models.DO_NOTHING)
    f_questionaire = models.ForeignKey(FilledQuestionaire, models.CASCADE)
    shown = models.BooleanField(blank=True, null=True)
    filled_in = models.DateTimeField(blank=True, null=True)
    time_taken = models.TimeField(blank=True, null=True)

    def __str__(self):
        return f"{self.module} {self.filled_in}"


class FilledQuestion(models.Model):
    id = models.BigAutoField(primary_key=True)
    question = models.ForeignKey(Question, models.DO_NOTHING)
    f_module = models.ForeignKey(FilledModule, models.CASCADE)
    shown = models.BooleanField(blank=True, null=True)
    filled_in = models.BooleanField(blank=True, null=True)
    last_edit = models.DateTimeField(blank=True, null=True)
    edits = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return f"{self.question} {self.last_edit}"
