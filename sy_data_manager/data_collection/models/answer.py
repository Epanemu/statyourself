from django.db import models
from . import FilledQuestion
from questionaires.models import QuestionChoice
from utils import remove_exponent

class Answer(models.Model):
    id = models.BigAutoField(primary_key=True)
    f_question = models.ForeignKey(FilledQuestion, models.CASCADE)
    answer_date = models.DateTimeField()

    def __str__(self):
        return f"{self.id} {self.answer_date}"


class AnswerNumber(models.Model):
    answer = models.OneToOneField(Answer, models.CASCADE, primary_key=True)
    value = models.DecimalField(max_digits=15, decimal_places=5, blank=True, null=True)

    class Meta:
       verbose_name = 'Number Answer'
       verbose_name_plural = 'Number Answers'

    def __str__(self):
        return f"{remove_exponent(self.value)}"


class AnswerChoice(models.Model):
    answer = models.OneToOneField(Answer, models.CASCADE, primary_key=True)
    choice = models.ForeignKey(QuestionChoice, models.CASCADE)

    class Meta:
       verbose_name = 'Choice Answer'
       verbose_name_plural = 'Choice Answers'

    def __str__(self):
        return f"{self.choice}"


class AnswerText(models.Model):
    answer = models.OneToOneField(Answer, models.CASCADE, primary_key=True)
    value = models.TextField(blank=True, null=True)

    class Meta:
       verbose_name = 'Text Answer'
       verbose_name_plural = 'Text Answers'

    def __str__(self):
        return f"{self.value}"


class AnswerDuration(models.Model):
    answer = models.OneToOneField(Answer, models.CASCADE, primary_key=True)
    value = models.TimeField(blank=True, null=True)

    class Meta:
       verbose_name = 'Duration Answer'
       verbose_name_plural = 'Duration Answers'

    def __str__(self):
        return f"{self.value}"


class AnswerTime(models.Model):
    answer = models.OneToOneField(Answer, models.CASCADE, primary_key=True)
    value = models.DateTimeField(blank=True, null=True)

    class Meta:
       verbose_name = 'Time Answer'
       verbose_name_plural = 'Time Answers'

    def __str__(self):
        return f"{self.value}"


class AnswerRange(models.Model):
    answer = models.OneToOneField(Answer, models.CASCADE, primary_key=True)
    lowest = models.DecimalField(max_digits=15, decimal_places=5, blank=True, null=True)
    highest = models.DecimalField(max_digits=15, decimal_places=5, blank=True, null=True)

    class Meta:
       verbose_name = 'Range Answer'
       verbose_name_plural = 'Range Answers'

    def __str__(self):
        return f"[{remove_exponent(self.lowest)}, {remove_exponent(self.highest)}]"
