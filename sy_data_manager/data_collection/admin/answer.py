from django.contrib import admin
from data_collection.models import Answer, AnswerDuration, AnswerNumber, AnswerChoice, AnswerRange, AnswerText, AnswerTime

@admin.register(Answer)
class AnswerAdmin(admin.ModelAdmin):
    model = Answer


@admin.register(AnswerNumber)
class AnswerNumberAdmin(admin.ModelAdmin):
    model = AnswerNumber


@admin.register(AnswerChoice)
class AnswerChoiceAdmin(admin.ModelAdmin):
    model = AnswerChoice


@admin.register(AnswerText)
class AnswerTextAdmin(admin.ModelAdmin):
    model = AnswerText


@admin.register(AnswerDuration)
class AnswerDurationAdmin(admin.ModelAdmin):
    model = AnswerDuration


@admin.register(AnswerTime)
class AnswerTimeAdmin(admin.ModelAdmin):
    model = AnswerTime


@admin.register(AnswerRange)
class AnswerRangeAdmin(admin.ModelAdmin):
    model = AnswerRange
