from django.contrib import admin
from data_collection.models import FilledQuestionaire, FilledModule, FilledQuestion

@admin.register(FilledQuestionaire)
class FilledQuestionaireAdmin(admin.ModelAdmin):
    model = FilledQuestionaire


@admin.register(FilledModule)
class FilledModuleAdmin(admin.ModelAdmin):
    model = FilledModule


@admin.register(FilledQuestion)
class FilledQuestionAdmin(admin.ModelAdmin):
    model = FilledQuestion
